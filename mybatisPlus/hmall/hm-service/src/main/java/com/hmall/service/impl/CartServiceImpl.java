package com.hmall.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmall.common.exception.BadRequestException;
import com.hmall.common.exception.BizIllegalException;
import com.hmall.common.utils.BeanUtils;
import com.hmall.common.utils.CollUtils;
import com.hmall.common.utils.UserContext;
import com.hmall.domain.dto.CartFormDTO;
import com.hmall.domain.dto.ItemDTO;
import com.hmall.domain.po.Cart;
import com.hmall.domain.po.Item;
import com.hmall.domain.vo.CartVO;
import com.hmall.mapper.CartMapper;
import com.hmall.service.ICartService;
import com.hmall.service.IItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements ICartService {

    private final IItemService itemService;

    @Override
    public void addItem2Cart(CartFormDTO cartFormDTO) {
        if(checkItemExists(cartFormDTO.getItemId(),UserContext.getUser())){
            lambdaUpdate().setSql("num = num + 1")
                    .eq(Cart::getItemId,cartFormDTO.getItemId())
                    .eq(Cart::getUserId,UserContext.getUser())
                    .update();
        }else {
            Long count = lambdaQuery().eq(Cart::getItemId, cartFormDTO.getItemId())
                    .eq(Cart::getUserId, UserContext.getUser())
                    .count();
            if (count>=10){
                throw new BizIllegalException("购物车商品数量超过限制！");
            }
            Cart cart = BeanUtil.toBean(cartFormDTO, Cart.class);
            cart.setUserId(UserContext.getUser());
            save(cart);
        }

    }

    @Override
    public List<CartVO> queryMyCarts() {
        List<Cart> carts = lambdaQuery().eq(Cart::getUserId, UserContext.getUser()).list();
        List<CartVO> cartVOS = BeanUtil.copyToList(carts, CartVO.class);
        if (CollUtils.isNotEmpty(cartVOS)){
            List<Long> collect = cartVOS.stream().map(CartVO::getItemId).collect(Collectors.toList());
            List<Item> items = itemService.listByIds(collect);
            Map<Long, Item> itemMap = items.stream().collect(Collectors.toMap(Item::getId, Function.identity()));
            cartVOS.forEach(cartVO -> {
                Item item = itemMap.get(cartVO.getItemId());
                cartVO.setImage(item.getImage());
                cartVO.setStock(item.getStock());
                cartVO.setStatus(item.getStatus());
            });
            return cartVOS;
        }
        return CollUtils.emptyList();
    }
    private boolean checkItemExists(Long itemId, Long userId) {
        Long count = lambdaQuery().eq(Cart::getItemId, itemId)
                .eq(Cart::getUserId, userId).count();
        return count > 0;
    }
    @Override
    public void removeByItemIds(Collection<Long> itemIds) {
        // 1.构建删除条件，userId和itemId
        QueryWrapper<Cart> queryWrapper = new QueryWrapper<Cart>();
        queryWrapper.lambda()
                .eq(Cart::getUserId, UserContext.getUser())
                .in(Cart::getItemId, itemIds);
        // 2.删除
        remove(queryWrapper);
    }
}
